# happyBirthday

#### 介绍
这里是一些生日快乐祝福网页的模板

#### 软件架构
这里都是一些html/css/javascript文件，直接下载到本地打开index.html文件即可开箱。


#### 安装教程

1.  右上角下载压缩包或者git clone到本地
2.  任选一个文件夹，打开index.html文件即可（如内有dist文件夹，则在dist文件夹中打开index.html文件）
3.  如有想要修改的内容，请用文本模式（如记事本，notepad++）打开html文件修改后保存

#### 使用说明

1.  如需部署，请查看B站视频，我的B站昵称是“ 小明今天编程了吗 ”
2.  
3.  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

